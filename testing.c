/*
Tiffany Chung (tchung12@jhu.edu)
600.120 Intermediate Programming
Assignment 4

* this is the main that I am using for incremental testing

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Game.c"


void test_printDeck(Game *);
void test_printNames(Game *);
void test_printHands(Game *);
void test_printPoints(Game *);

int main (void) {

    Game one;
    initPlayers(&one);
    createDeck(&one);
    //test_printDeck(&one);
    //printf("deck created\n");
    //test_printNames(&one);
    dealRound(&one);
    //printf("\nshuffled deck\n");
    //test_printHands(&one);
    //test_printPoints(&one);

    playRound(&one);
    


}

void test_printDeck(Game * g) {
    int i;
    for (i = 0; i < 60; i++) {
        printf("suit: %s, value: %d\n", g->deck[i].suit, g->deck[i].value);
    }

}

void test_printNames(Game * g) {
    int i;
    for (i = 0; i < 4; i++) {
        printf("player %d is: %s\n", i + 1, g->players[i].name);
    }
    printf("%s is the current dealer\n", g->players[g->dealer].name);
    printf("%s is the current leader\n", g->players[g->leader].name);
}

void test_printHands(Game * g) {
    int i;
    for (i = 0; i < 4; i++) {
        printHand(&g->players[i]);
    }
}

void test_printPoints(Game * g) {
    printPoints(g);
}