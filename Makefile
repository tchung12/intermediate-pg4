#Tiffany Chung tchung12@jhu.edu 925-383-6689
#600.120 Intermediate Programming
#Assignment 4: Spades Game


CFLAGS = -std=c99 -pedantic -Wall -Wextra -O
CC = gcc


pg4: pg4.c
	$(CC) $(CFLAGS) pg4.c -o pg4

clean:
	rm -f *.o pg4 a.out 


