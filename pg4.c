/*
Tiffany Chung (tchung12@jhu.edu) 925-383-6689
600.120 Intermediate Programming
Assignment 4: Spades Game
*/


#include "Game.c"

int loadGame(Game *, FILE *);
int saveGame(Game *, FILE *);

int main (int argc, char * argv[]) {

    if (argc == 0) {
        fprintf(stderr, "No file inputted. Goodbye.\n");  
    }
    FILE * fptr = fopen(argv[1],"r + b");

    Game one;
    int s = 0, a= 2;
    
    if (fptr == NULL) {
    // file does not exist - create it and reopen
        printf("File does not exist. Creating new fie.\n");
        fptr = fopen(argv[1], "w + b");
        s = fclose(fptr);
        fptr = fopen(argv[1], "r + b");
        if (fptr == NULL) 
            return(0);
    }
    else {
        s = loadGame(&one, fptr);
        if (s) {
            printf("Loading saved game...\n");
            printf("Saved scores...\n");
            printPoints(&one);
            a = 3;
        }
        else {
            printf("Error loading game...\nNew game created.\n\n");
            a = 2;
        }
    }

    while (a) {
        if (a == 1) {
            printf("Saving and exiting.\n");
            a = 0;
            s = saveGame(&one, fptr);
            if (s) {
                printf("Game saved. Goodbye!\n");
            }
        }
        else if (a == 2) {
            one.round = 1;
            initPlayers(&one);
            createDeck(&one);
            dealRound(&one);
            printf("Round %d\n", one.round);
            a = playRound(&one);
            one.round++; 
        }
        else if (a == 3) {
            printf("Round %d\n", one.round);
            dealRound(&one);
            a = playRound(&one); 
            one.round++;
        }
    }

    fclose (fptr);
    return 0;

}


int loadGame(Game * one, FILE * fptr) {
    int s;

    s = fread(&one->deck, sizeof(Card), 60, fptr);
    s = fread(&one->players, sizeof(Player), 4, fptr);
    s = fread(&one->leader, sizeof(int), 1, fptr);
    s = fread(&one->dealer, sizeof(int), 1, fptr);
    s = fread(&one->round, sizeof(int), 1, fptr);

    for (int i = 0; i < 4; i++) {
        one->players[i].hand = malloc(sizeof(CNode));
        one->players[i].hand->next = NULL;
    }
    
    s = fseek(fptr, 0, SEEK_SET);
    s++;
    return s;
}

int saveGame(Game * one, FILE * fptr) {
    int s;
    
    s = fwrite(&one->deck, sizeof(Card), 60, fptr);
    s = fwrite(&one->players, sizeof(Player), 4, fptr);
    s = fwrite(&one->leader, sizeof(int), 1, fptr);
    s = fwrite(&one->dealer, sizeof(int), 1, fptr);
    s = fwrite(&one->round, sizeof(int), 1, fptr);

    s = fseek(fptr, 0, SEEK_SET);
    s++;

    return s;
}
