/*
Tiffany Chung (tchung12@jhu.edu) 925-383-6689
600.120 Intermediate Programming
Assignment 4: Spades Game
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define GAME_H


#include "Player.c"



typedef struct {
    Card deck[60];
    Player players[4];
    int leader;
    int dealer;
    int round;
} Game;

/* Ask user of they would like to continue.
*/
int askContinue(void);

/** Initialize the game, getting using input for the player's names.
 */
void initPlayers(Game *);

/** Initialize a full deck of cards for the game to use.
 */
void createDeck(Game *);

/** Shuffle and deal the deck round-robin to all the players.
 */
void dealRound(Game *);

/** Print the names and points of all the players.
 */
void printPoints(Game *);

/** Play a round (hand) of the game, from dealing through to printing points.
 */
int playRound(Game *); 

/** Play only one trick of the game.
 */
void playTrick(Game *, int);
