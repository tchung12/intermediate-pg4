/*
Tiffany Chung (tchung12@jhu.edu) 925-383-6689
600.120 Intermediate Programming
Assignment 4: Spades Game
*/


#include "Game.h"

int askContinue(void) {
    char input[2];
    int m, choice;
    //TODO: ask if want to save, continue, or start a new game
    printf("Choose from the following:\n\tA) save and quit\n\tB) start new game\n\tC) continue\n");
    m = scanf("%s", input);
    input[0] = toupper(input[0]);

    if (strcmp(input, "A") == 0) {
        choice = 1;
    }
    else if (strcmp(input, "B") == 0) {
        choice = 2;
    }
    else if (strcmp(input, "C") == 0) {
        choice = 3;
        printf("Would you like to play another round? (Y/N)\n");      
        m = scanf("%s", input);
        m++;
        input[0] = toupper(input[0]);

        if (strcmp(input, "Y") == 0) {
            choice = 3;
        }
        else {
            choice = 0;
        }
    }
    else {
        printf("Invalid choice. Goodbye.\n");
        choice = 0;
    }

    return choice;
}


/** Initialize the game, getting using input for the player's names.
 */
void initPlayers(Game * g) {
    printf("Welcome to Spades Game.\n");
    int i, m;
    char pname[20];
    
    for (i = 0; i < 4; i++) {
        printf("Name for Player %d: ", i + 1);
        m = scanf("%s", pname);
        strcpy(g->players[i].name, pname);
        g->players[i].number = i + 1;
        g->players[i].points = 0;
        g->players[i].hand = malloc(sizeof(CNode));
        g->players[i].hand->next = NULL;
        g->players[i].bid = 0;
        g->players[i].cpts = 0;
    }

    g->dealer = 0;
    g->leader = 1;
    g->round = 1;
    m++;
}

/** Initialize a full deck of cards for the game to use.
 */
void createDeck(Game * g) {
    int i, j;
    for (j = 0; j < 15; j++) {
        strcpy(g->deck[j].suit, "○");
        g->deck[j].value = j + 1;
    }
    for (i = 0, j = 15; i < 15; i++, j++) {
        strcpy(g->deck[j].suit, "□");
        g->deck[j].value = i + 1;
    }
    for (i = 0, j = 30; i < 15; i++, j++) {
        strcpy(g->deck[j].suit, "◇");
        g->deck[j].value = i + 1;
    }
    for (i = 0, j = 45; i < 15; i++, j++) {
        strcpy(g->deck[j].suit, "△");
        g->deck[j].value = i + 1;
    }
}

/** Shuffle and deal the deck round-robin to all the players.
 */
void dealRound(Game * g) {
    int r = 0, i;
    int rh[] = {0, 0, 0, 0};
    srand(time(NULL));    
    for (i = 0; i < 60;) {
        r = rand() % 4;
        if (rh[r] < 15) {
            addCard(g->players[r].hand, &g->deck[i]);
            rh[r] = rh[r] + 1;
            i++;
        }
    }
}

/** Print the names and points of all the players.
 */
void printPoints(Game * g) {
    int i;
    printf("\n======== Scoreboard ========\n");
    for (i = 0; i < 4; i++) {
        printf("\t%s has %d points.\n", g->players[i].name, g->players[i].points);
    }
    printf("============================\n");
}

/** Play a round (hand) of the game, from dealing through to printing points.
 */
int playRound(Game * g) {
    int i, b, m, c, d, f;
    char e;

    // prompt for bid
    for (i = 0; i < 4; i++) {
        g->players[i].cpts = 0;
        printf("%s, please enter your bid:\n", g->players[i].name);
        m = scanf("%d%c", &b, &e);
        g->players[i].bid = b;
        if ((b > 16) || (b < 0) || (e != '\n')) {
            i--;
            printf("Invalid bid, try again.\n");
        }
    }

    for (i = 1; i < 16; i++) {
        playTrick(g, i);
        printf("New leader is %s.\n", g->players[g->leader].name);
        printf("\n\n");
    }

    // calculate bid to points
    for (i = 0; i < 4; i++) {
        c = g->players[i].bid;
        d = g->players[i].cpts;
        if (d >= c) {
            g->players[i].cpts = (d * 10) + (d - c);
        }
        else {
            g->players[i].cpts = d - c;
        }
        g->players[i].points += g->players[i].cpts;
    }

    printPoints(g);
    g->dealer++;
    m++;

    f = askContinue();
    
    return f;
} 

/** Play only one trick of the game.
 */
void playTrick(Game * g, int r) {
    int i = g->leader, j, m;
    int leaderValue = 0;
    int newleader = i;
    char leaderSuit[] = "○";
    char input[2];

    printf("\n\n----- Trick %d -----\n", r);

    for (j = 0; j < 4; j++) {

        printf("It is now %s's Turn.\n", g->players[i].name);
        printHand(&g->players[i]);
        printf("Choose a card to play using the letter.\n");
        m = scanf("%s", input);

        Card * current = play(&g->players[i], *input);

        // set the leader's card to beat
        if (j == 0) {
            leaderValue = current->value;
            strcpy(leaderSuit, current->suit);
            newleader = i;
        }     
        else {

            // if same suit and value is higher
            if ((strcmp(current->suit, leaderSuit) == 0) && (current->value > leaderValue)) {
                leaderValue = current->value;
                newleader = i;
            }
            // else if circles and not leading suit, and value is higher
            else if ((strcmp(current->suit, "○") == 0) && (current->value > leaderValue)) {
                leaderValue = current->value;
                newleader = i;
            }
            // anything else, do nothing
            else {
            }         
        }
        
        printf("\n\n*** Current card to beat: %d of %s ***\n\n", leaderValue, leaderSuit);
        i = (i + 1) % 4;

    }
    // determine the winner of the trick
    printf("%s takes the trick.\n", g->players[newleader].name);
    // give points to winner of trick
    g->players[newleader].cpts += 1;
    g->leader = newleader;
    m++;
}

