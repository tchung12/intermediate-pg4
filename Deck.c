#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DECK_H"



void initDeck(ShapesDeck d) {
    int i;

    for (int i = 0; i < 15; i++) {
        // make new card of suit Circle
        // give d.whole a pointer to that card
        Card newCard;
        newCard.suit = "Circle";
        newCard.value = i;
        d.whole[i] = newCard;
    }
    for (int i = 15; i < 30; i++) {
        // make new card of suit Squares
        Card newCard;
        newCard.suit = "Squares";
        newCard.value = i;
        d.whole[i] = newCard;
    }
    for (int i = 30; i < 45; i++) {
        // make new card of suit Diamonds
        Card newCard;
        newCard.suit = "Triangles";
        newCard.value = i;
        d.whole[i] = newCard;
    }
    for (int i = 45; i < 60; i++) {
        // make new card of suit Diamonds
        Card newCard;
        newCard.suit = "Diamonds";
        newCard.value = i;
        d.whole[i] = newCard;
    }
}



void shuffleDeck(ShapesDeck d) {
    int r, i;
    for (i = 0; i < 60, i++) {
        r = rand(60);
        Card * temp = d.whole[i];
        d.whole[i] = d.whole[r];
        d.whole[r] = temp;
    }

}