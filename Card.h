/*
Tiffany Chung (tchung12@jhu.edu) 925-383-6689
600.120 Intermediate Programming
Assignment 4: Spades Game
*/

#define CARD_H

typedef struct {
    char suit[10];
    int value;
} Card;


/*
struct CNode{
    Card * cptr;
    struct CNode * next;
} ;

typedef struct CNode CNode;



// Clear a card list.

void clear(CNode **hptr);

*/