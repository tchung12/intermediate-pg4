/*
Tiffany Chung (tchung12@jhu.edu) 925-383-6689
600.120 Intermediate Programming
Assignment 4: Spades Game
*/

#define PLAYER_H
#include "Card.h"


struct CNode{
    Card * cptr;
    struct CNode * next;
} ;

typedef struct CNode CNode;

typedef struct {
    char name[21];
    int number;
    int points;
    CNode * hand;
    int bid;
    int cpts;

} Player;




/** Display the name of the player and the cards in the hand of a
    player, lettering the cards starting with 'a' as part of the
    display.
 */
void printHand(const Player *);

/** Play a card selected by letter (comes out of hand).
    Return pointer to the Card played.
 */
Card * play(Player *, char letter);

/** Add a card to a Player's hand.
 */
void addCard(CNode *, Card *);

/** This player takes the trick just played by all.

void takeTrick(Player *, CNode * trick);
*/
