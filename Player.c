/*
Tiffany Chung (tchung12@jhu.edu) 925-383-6689
600.120 Intermediate Programming
Assignment 4: Spades Game
*/


#include "Player.h"



/** Display the name of the player and the cards in the hand of a
    player, lettering the cards starting with 'a' as part of the
    display.
 */
void printHand(const Player * p) {
    int i = 0;
    char lettering[] = "abcdefghijklmno";
    printf("%s's Hand:\n", p->name);

    CNode * head = p->hand;

    while (head->next != NULL) {
        char * suit = head->next->cptr->suit;
        printf("%c) %d of %s\n", lettering[i], head->next->cptr->value, suit);
        head = head->next;
        i++;
    }
}

/** Play a card selected by letter (comes out of hand).
    Return pointer to the Card played.
 */
Card * play(Player * p, char letter) {

    int select = letter - 'a';
    int i;

    CNode * hold = p->hand->next;

    if (select == 0) {
        p->hand->next = p->hand->next->next;
        return hold->cptr;
    }

    for (i = 0; i < select - 1; i++) {
        hold = hold->next;
    }
    CNode * played = hold->next;

    hold->next = played->next;

    Card * curr = played->cptr;

    free(played);
    return curr;
}

/** Add a card to a Player's hand.
 */
void addCard(CNode * h, Card * c) {
    CNode * new = malloc(sizeof(CNode));
    new->cptr = c;
    new->next = NULL;

    if (h->next == NULL) {
        h->next = new;
    }
    else {
        while (h->next != NULL) {
            h = h->next;
        }
        h->next = new;
    }

}

/** This player takes the trick just played by all.
void takeTrick(Player * p, CNode * trick) {

}
*/
